<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/animate.min.css',
        'css/bootstrap.min.css',
        'css/brand.css',
        'css/card.css',
        'css/filtir.css',
        'css/harid.css',
        'css/jquery.mCustomScrollbar.min.css',
        'css/jquery-ui.css',
        'css/meanmenu.css',
        'css/nice-select.css',
        'css/normalize.css',
        'css/owl.carousel.min.css',
        'css/responsive.css',
        'css/slick.css',
        'css/style.css',
    ];
    public $js = [
       // 'js/jquery.min.js',
        'js/jquery-3.0.0.min.js',

        'js/popper.min.js',
        'js/bootstrap.bundle.min.js',
        'js/jquery.mCustomScrollbar.concat.min.js',
        'js/custom.js',
        'js/plugin.js',
        'js/card.js',
        //'js/owl.carousel.js',
    ];
    public $depends = [
      // 'yii\web\YiiAsset',
      // 'yii\bootstrap\BootstrapAsset',
    ];
}
