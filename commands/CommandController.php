<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

class CommandController extends Controller
{
    public function actionIndex($msg = 'Yii custom command')
    {
        echo  $msg . "\n";

        return ExitCode::OK;
    }
}