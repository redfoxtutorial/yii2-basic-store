<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'role' => [
        ['key' => 0, 'name' => 'User'],
        ['key' => 1, 'name' => 'Admin'],
    ],

    'gender' => [
        ['key' => 1, 'name' => 'Male'],
        ['key' => 2, 'name' => 'Female'],
    ],

    'status' => [
        0 => 'Inactive',
        1 => 'Active',
    ]
];
