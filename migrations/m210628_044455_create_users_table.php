<?php

use yii\db\Migration;
//use Yii;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m210628_044455_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'full_name'=>$this->string(255),
            'username'=>$this->string(50)->unique(),
            'email'=>$this->string(255)->unique(),
            'password'=>$this->string(255),
            'role' => $this->integer(2)->defaultValue(0)->comment('0 - users, 1 - admins') ,
            'birthday'=>$this->dateTime(),
            'gender'=>$this->string()->null(),
            'photo'=>$this->string()->null(),
        ]);

        $this->insert('users',[
            'full_name' => 'Admin Admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'role' => 1
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
