<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%size}}`.
 */
class m210628_044707_create_size_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%size}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%size}}');
    }
}
