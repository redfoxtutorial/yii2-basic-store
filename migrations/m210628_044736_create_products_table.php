<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 */
class m210628_044736_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'category_id' => $this->integer()->notNull(),
            'size_ids' => $this->string()->null(),
            'color_ids' => $this->string()->null(),
            'old_price' => $this->float(),
            'new_price' => $this->float(),
            'currency_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->defaultValue(0),
            'discount' => $this->float(),
            'delivery' => $this->integer()->defaultValue(1)->comment('0 - yetkazib berish yo\'q, 1 - yetkazib berish bor'),
            'status' => $this->integer()->defaultValue(1)->comment('1 - yangi, 2 - aksiyadagi, 3 - ommalashgan'),
            'description' => $this->text()->null(),
        ]);

        $this->createIndex(
            'idx-products-category_id',
            'products',
            'category_id'
        );

        $this->addForeignKey(
            'fk-products-category_id',
            'products',
            'category_id',
            'categories',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-products-currency_id',
            'products',
            'currency_id'
        );

        $this->addForeignKey(
            'fk-products-currency_id',
            'products',
            'currency_id',
            'currencies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products}}');
    }
}
