<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%card}}`.
 */
class m210628_044810_create_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%card}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->defaultValue(0),
            'date' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-card-user_id',
            'card',
            'user_id'
        );

        $this->addForeignKey(
            'fk-card-users_id',
            'card',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-card-product_id',
            'card',
            'product_id'
        );

        $this->addForeignKey(
            'fk-card-product_id',
            'card',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%card}}');
    }
}
