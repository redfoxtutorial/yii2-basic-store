<?php

namespace app\models;

use app\modules\admin\models\CardModel;
use app\modules\admin\models\OrdersModel;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $full_name
 * @property string|null $username
 * @property string|null $email
 * @property string|null $password
 * @property string|null $birthday
 * @property string|null $gender
 * @property string|null $photo
 *
 * @property CardModel[] $cards
 * @property OrdersModel[] $orders
 */
class UsersModel extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday'], 'safe'],
            [['full_name', 'email', 'password', 'gender', 'photo'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 50],
            [['username'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'photo' => 'Photo',
        ];
    }

    /**
     * Gets query for [[Cards]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(CardModel::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(OrdersModel::className(), ['user_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        return static::find()->where(['id' => $id])->one();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return null;
    }

    public function validateAuthKey($authKey)
    {
        return null;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public function getRole()
    {
        $role = 'User';
        $params = Yii::$app->params['role'];
        foreach ($params as $pr){
            if($pr['key'] === $this->role){
                $role = $pr['name'];
            }
        }
        return $role;
    }
}
