<?php
namespace app\modules\admin;

use Yii;
use yii\base\Module;

class AdminModule extends Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init()
    {
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'];
        parent::init();
    }
}