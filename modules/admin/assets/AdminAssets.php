<?php
namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class AdminAssets extends AssetBundle
{
    public $basePath = '@webroot';
    //public $sourcePath = '@app/modules/admin/web';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',
    ];

    public $js = [

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}