<?php
namespace app\modules\admin\components;

use yii\web\BadRequestHttpException;
use yii\web\Controller;
use Yii;

class AController extends Controller
{
    /**
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity && Yii::$app->user->identity->getRole() === 'Admin') {
            return parent::beforeAction($action);
        }
        return $this->goHome();
    }
}