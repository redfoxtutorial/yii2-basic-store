<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\ProductsModel;
use Yii;
use app\modules\admin\models\ProductImages;
use app\modules\admin\models\ProductImagesSearch;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductImagesController implements the CRUD actions for ProductImages model.
 */
class ProductImagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductImages models.
     * @return mixed
     */
    public function actionIndex($product_id)
    {
        $images = ProductImages::find()->where(['product_id' => $product_id])->all();
        $product = ProductsModel::findOne($product_id);

        return $this->render('index', [
            'images' => $images,
            'product' => $product
        ]);
    }

    /**
     * Displays a single ProductImages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($product_id)
    {
        $model = new ProductImages();
        if (Yii::$app->request->isPost)
        {

            $existImage = ProductImages::find()->where(['product_id' => $product_id])->all();
            if(!empty($existImage)){
                foreach ($existImage as $e_img){
                    if(file_exists('uploads/'.$e_img->image)){
                        unlink('uploads/' .$e_img->image);
                    }
                    $e_img->delete();
                }
            }
            $files = UploadedFile::getInstances($model, 'image');

            foreach ($files as $file){
                 $model = new ProductImages();
                 $model->image = $file;
                 $model->product_id = $product_id;
                 $new_name = Yii::$app->security->generateRandomString().'.'.$model->image->extension;
                 if ($model->image) {
                     $model->image->saveAs(Yii::getAlias('@webroot') .'/uploads/' . $new_name);
                 }
                 $model->image = $new_name;
                 $model->save(false);
             }
            return $this->redirect(['index', 'product_id' => $model->product_id]);
        }
        $product = ProductsModel::findOne($product_id);
        return $this->render('create', [
            'model' => $model,
            'product' => $product
        ]);
    }

    /**
     * Updates an existing ProductImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductImages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionEditImage($id){

        $model = ProductImages::findOne($id);
        if(Yii::$app->request->isPost){
            $file = UploadedFile::getInstance($model, 'image');

            if($file){
                if(file_exists('uploads/'.$model->image)){
                    unlink('uploads/' .$model->image);
                }
            }
            $model->image = $file;
            $new_name = Yii::$app->security->generateRandomString().'.'.$model->image->extension;
            if ($model->image) {
                $model->image->saveAs(Yii::getAlias('@webroot') .'/uploads/' . $new_name);
            }
            $model->image = $new_name;
            $model->save(false);
            return $this->redirect(['index', 'product_id' => $model->product_id]);
        }
        return $this->render('edit-image',[
            'model' => $model
        ]);
    }
}

