<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\CategoriesModel;
use app\modules\admin\models\SizeModel;
use app\modules\admin\models\Currencies;
use app\modules\admin\models\ColorModel;
use Yii;
use app\modules\admin\models\ProductsModel;
use app\modules\admin\models\ProductsModelSearch;
use app\modules\admin\components\AController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductsController implements the CRUD actions for ProductsModel model.
 */
class ProductsController extends AController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductsModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductsModel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductsModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductsModel();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->size_ids = implode(',',$_POST['ProductsModel']['size_ids']);
            $model->color_ids = implode(',',$_POST['ProductsModel']['color_ids']);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $categories = ArrayHelper::map(CategoriesModel::find()->all(),'id','name');
        $sizes = ArrayHelper::map(SizeModel::find()->all(),'id','name');
        $currencies = ArrayHelper::map(Currencies::find()->all(), 'id', 'name');
        $colors = ArrayHelper::map(ColorModel::find()->all(), 'id', 'name');
        return $this->render('create', [
            'model' => $model,
            'categories' => $categories,
            'sizes' => $sizes,
            'currencies' => $currencies,
            'colors' => $colors,
        ]);
    }

    /**
     * Updates an existing ProductsModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->size_ids = implode(',',$_POST['ProductsModel']['size_ids']);
            $model->color_ids = implode(',',$_POST['ProductsModel']['color_ids']);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        if($model->size_ids){
            $model->size_ids = explode(',',$model->size_ids);
        }
        if($model->color_ids){
            $model->color_ids = explode(',',$model->color_ids);
        }
        $categories = ArrayHelper::map(CategoriesModel::find()->all(),'id','name');
        $sizes = ArrayHelper::map(SizeModel::find()->all(),'id','name');
        $currencies = ArrayHelper::map(Currencies::find()->all(), 'id', 'name');
        $colors = ArrayHelper::map(ColorModel::find()->all(), 'id', 'name');
        return $this->render('update', [
            'model' => $model,
            'categories' => $categories,
            'sizes' => $sizes,
            'currencies' => $currencies,
            'colors' => $colors,
        ]);
    }

    /**
     * Deletes an existing ProductsModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductsModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductsModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductsModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
