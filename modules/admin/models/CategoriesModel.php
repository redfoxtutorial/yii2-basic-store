<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "Categories".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $status
 *
 * @property Products[] $products
 */
class CategoriesModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'],'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[products]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getProducts()
    // {
    //     return $this->hasMany(products::className(), ['category_id' => 'id']);
    // }

    public function getStatusString()
    {
        $t = '';
        if($this->status == 1){
            $t = 'Inactive';
        }elseif ($this->status == 2){
            $t = 'Active';
        }
        return $t;
    }
}
