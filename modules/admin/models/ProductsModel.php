<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string|null $name
 * @property int $category_id
 * @property string|null $size_ids
 * @property string|null $color_ids
 * @property float|null $old_price
 * @property float|null $new_price
 * @property int $currency_id
 * @property int|null $quantity
 * @property float|null $discount
 * @property int|null $delivery 0 - yetkazib berish yo'q, 1 - yetkazib berish bor
 * @property int|null $status 1 - yangi, 2 - aksiyadagi, 3 - ommalashgan
 * @property string|null $description
 *
 * @property CardModel[] $cards
 * @property OrdersModel[] $orders
 * @property ProductImages[] $productImages
 * @property CategoriesModel $category
 * @property Currencies $currency
 */
class ProductsModel extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'currency_id'], 'required'],
            [['category_id', 'currency_id', 'quantity', 'delivery', 'status'], 'integer'],
            [['old_price', 'new_price', 'discount'], 'number'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriesModel::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currencies::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category',
            'size_ids' => 'Sizes',
            'color_ids' => 'Colors',
            'old_price' => 'Old Price',
            'new_price' => 'New Price',
            'currency_id' => 'Currency',
            'quantity' => 'Quantity',
            'discount' => 'Discount',
            'delivery' => 'Delivery',
            'status' => 'Status',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Cards]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(CardModel::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(OrdersModel::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[ProductImages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CategoriesModel::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currencies::className(), ['id' => 'currency_id']);
    }

    public function getSizeName() {
        $sizes = [];
        $name = '';
        if(!empty($this->size_ids))
        {
            $ids = explode(',', $this->size_ids);
            $sizes = SizeModel::findAll($ids);
            foreach ($sizes as $size){
                $name .= $size->name.', ';
            }
        }
        return $name;
    }
    public function getColorName() {
        $colors = [];
        $name = '';
        if(!empty($this->color_ids))
        {
            $ids = explode(',', $this->color_ids);
            $colors = ColorModel::findAll($ids);
            foreach ($colors as $colors){
                $name .= $colors->name.', ';
            }
        }
        return $name;
    }
}
