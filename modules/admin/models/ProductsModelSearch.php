<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\ProductsModel;

/**
 * ProductsModelSearch represents the model behind the search form of `app\modules\admin\models\ProductsModel`.
 */
class ProductsModelSearch extends ProductsModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'currency_id', 'quantity', 'delivery', 'status'], 'integer'],
            [['name', 'size_ids', 'color_ids', 'description'], 'safe'],
            [['old_price', 'new_price', 'discount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductsModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'old_price' => $this->old_price,
            'new_price' => $this->new_price,
            'currency_id' => $this->currency_id,
            'quantity' => $this->quantity,
            'discount' => $this->discount,
            'delivery' => $this->delivery,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'size_ids', $this->size_ids])
            ->andFilterWhere(['like', 'color_ids', $this->color_ids])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
