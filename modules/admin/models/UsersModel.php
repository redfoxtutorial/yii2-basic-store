<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $full_name
 * @property string|null $username
 * @property string|null $email
 * @property string|null $password
 * @property string|null $birthday
 * @property string|null $gender
 * @property string|null $photo
 *
 * @property Card[] $cards
 * @property Orders[] $orders
 */
class UsersModel extends ActiveRecord
{
    /**
     * {scenarios}
     */
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_RESET_PASSWORD = 'reset_password';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday'], 'safe'],
            [['password'],'required', 'on' => self::SCENARIO_INSERT],
            [['full_name','username', 'email','role'], 'required'],
            [['full_name', 'email', 'password','gender'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 50],
            [['username'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'photo' => 'Photo',
        ];
    }

    /**
     * Gets query for [[Cards]].
     *
     * @return ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(CardModel::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[orders]].
     *
     * @return ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(OrdersModel::className(), ['user_id' => 'id']);
    }
}
