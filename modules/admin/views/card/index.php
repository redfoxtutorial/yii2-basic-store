<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CardModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Card Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-model-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'product_id',
            'quantity',
            'date',

            // ['class' => 'yii\grid\ActionColumn'],

            [
                'format'=>'raw',
                'value' => function($data){
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view','id'=>$data->id], ['class'=>'btn btn-info btn-xs']);
                       
                }
            ],

        ],
    ]); ?>


</div>
