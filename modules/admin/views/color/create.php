<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ColorModel */

$this->title = 'Create Color Model';
$this->params['breadcrumbs'][] = ['label' => 'Color Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
