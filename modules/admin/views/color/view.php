<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ColorModel */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Color Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="color-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <td><?= $model->id ?></td>
        </tr>
        <tr>
            <th>Name</th>
            <td><?= $model->name?></td>
        </tr>
        <tr>
            <th>Code</th>
            <td><?= $model->code ?></td>
        </tr>
        <tr>
            <th>View</th>
            <td><span style="border-radius: 50%; background-color: <?=$model->code?>; display: block; height: 15px; width: 15px"> </span></td>
        </tr>
    </table>
    <?/*= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'code',
        ],
    ]) */?>

</div>
