<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\OrdersModel */

$this->title = 'Create orders Model';
$this->params['breadcrumbs'][] = ['label' => 'orders Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
