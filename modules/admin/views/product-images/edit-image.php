<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ProductImages */

$this->title = 'Product Images';
$this->params['breadcrumbs'][] = ['label' => 'Product Images', 'url' => ['/admin/product-images','product_id' => $model->product_id]];
$this->params['breadcrumbs'][] = 'Edit';
$this->params['breadcrumbs'][] = $model->id;
?>

<div class="product-images-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'image')->fileInput(['accept' => 'image/*']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
