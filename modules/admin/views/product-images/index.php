<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $images app\modules\admin\models\ProductImages */
/* @var $product app\modules\admin\models\ProductsModel */

$this->title = 'Product Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-images-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Product Images', ['create', 'product_id' => $product->id ], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <table class="table table-bordered">
        <tbody>
            <tr>
                <?php foreach ($images as $image):?>
                    <td> <img width="300px" src="/uploads/<?= $image->image ?>"></td>
                    <td>
                        <?= Html::a('Edit',['product-images/edit-image','id' => $image->id],['class'=>'btn btn-warning']) ?>
                    </td>
                <?php endforeach;?>
            </tr>
        </tbody>
    </table>
<!--    --><?//= GridView::widget([
//        'dataProvider' => $dataProvider,
//        //'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//            'product_id',
//            'image',
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]); ?>


</div>
