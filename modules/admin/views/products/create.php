<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ProductsModel */
/* @var $categories array */
/* @var $sizes array */
/* @var $currencies array */
/* @var $colors array */

$this->title = 'Create products Model';
$this->params['breadcrumbs'][] = ['label' => 'products Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'sizes' => $sizes,
        'currencies' => $currencies,
        'colors' => $colors,
    ]) ?>

</div>
