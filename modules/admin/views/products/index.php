<?php

use app\modules\admin\models\SizeModel;
use app\modules\admin\models\Currencies;
use app\modules\admin\models\CategoriesModel;
use app\modules\admin\models\ColorModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProductsModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'products Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-model-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create products Model', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'category_id',
                'filter' => Html::activeDropDownList(
                        $searchModel,'category_id',
                        ArrayHelper::map(CategoriesModel::find()->all(),'id','name'),
                        ['class'=>'form-control','prompt' => 'Select']
                ),
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return  $searchModel->category->name;

                },
            ],
            [
                'attribute' => 'size_ids',
                'filter' => Html::activeDropDownList(

                        $searchModel,'size_ids',
                        ArrayHelper::map(SizeModel::find()->all(),'id','name'),
                        ['class'=>'form-control','prompt' => 'Select']

                ),
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return  $searchModel->getSizeName();
                },
            ],
            [
                'attribute' => 'color_ids',
                'filter' => Html::activeDropDownList(

                        $searchModel,'color_ids',
                        ArrayHelper::map(ColorModel::find()->all(),'id','name'),
                        ['class'=>'form-control','prompt' => 'Select']

                ),
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return  $searchModel->getColorName();
                },
            ],
            //'old_price',
            //'new_price',
            [
                'attribute' => 'currency_id',
                'filter' => Html::activeDropDownList(

                        $searchModel,'currency_id',
                        ArrayHelper::map(Currencies::find()->all(),'id','name'),
                        ['class'=>'form-control','prompt' => 'Select']
                ),
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return  $searchModel->currency->name;
                },
            ],
            [
                    'attribute' => 'Image',
                    'format' => 'raw',
                    'value' => function($data){
                        return
                            Html::a('Edit',['/admin/product-images','product_id' => $data->id],['class'=>'btn btn-success btn-xs']);
                    }
            ],


            //'quantity',
            //'discount',
            //'delivery',
            //'status',
            //'description:ntext',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'format'=>'raw',
                'value' => function($data){
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view','id'=>$data->id], ['class'=>'btn btn-info btn-xs']).' '.
                        Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$data->id], ['class'=>'btn btn-success btn-xs']).' '.
                        Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $data->id], [
                            'class' => 'btn btn-danger btn-xs',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                }
            ],

        ],
    ]); ?>
</div>