<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ProductsModel */
/* @var $categories array */
/* @var $sizes array */

$this->title = 'Update products Model: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'products Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="products-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'sizes' => $sizes,
        'currencies' => $currencies,
        'colors' => $colors,
    ]) ?>

</div>
