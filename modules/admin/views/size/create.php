<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\SizeModel */

$this->title = 'Create Size Model';
$this->params['breadcrumbs'][] = ['label' => 'Size Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="size-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
