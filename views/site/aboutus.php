<div class="brand_color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titlepage">
                    <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Biz har doim siz bilan</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- about -->
<div class="about">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 co-sm-l2">
                <div class="about_img" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <figure><img src="/images/about.png" alt="img" /></figure>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-7 co-sm-l2">
                <div class="about_box">
                    <span data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Bizning do'konimiz</span>
                    <p data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti beatae sit sapiente repudiandae velit consectetur distinctio incidunt sequi, architecto vel!</p>

                </div>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-7 co-sm-l2">
                <div class="about_box_ ">
                    <span data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Bizning do'konimiz</span>
                    <p data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae, ex! Tempore est nihil odit aliquam ad eum id quas, accusantium voluptatem veritatis deserunt harum magnam earum voluptatibus? Nesciunt, rerum non?</p>

                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-5 co-sm-l2">
                <div class="about_img" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <figure><img src="/images/about.png" alt="img" /></figure>
                </div>
            </div>
        </div>
    </div>
</div>
