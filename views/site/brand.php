<div class="brand_color">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage">
                        <h2>Tanlangan mahsulot</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5 mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="images p-3">
                                <div class="text-center p-4" data-aos="flip-left"> <img id="main-image" src="/web/images/images/a32.jpeg" width="450" > </div>
                                <div class="row thumbnail text-center" style="border-style: none;">
                                <img onclick="change_image(this)" data-aos="flip-left" src="/images/images/a32 red.jpg" width="200">
                                <img onclick="change_image(this)" data-aos="flip-right" src="/images/images/a32 blue.jfif" width="200">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product p-4">
                                <div class="d-flex justify-content-between align-items-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                    <div class="d-flex align-items-center"> <a href="index.html" ><i class="fa fa-long-arrow-left"></i> <span class="ml-1">Orqaga</span></a>  </div> 
                                    <i style="color: red !important; " class="fa fa-shopping-cart fa-2x text-muted"></i>
                                </div>
                                <div class="mt-4 mb-3"> 
                                    <span class="text-uppercase text-muted brand_a" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Samsung</span>
                                    <h5 class="text-uppercase"  data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Samsung Galaxy A32 4/64GB Awesome Black</h5>
                                    <ul class="d-flex" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                        <li><img src="/images/star.png" alt=""></li>
                                        <li><img src="/images/star.png" alt=""></li>
                                        <li><img src="/images/star.png" alt=""></li>
                                        <li><img src="/images/star.png" alt=""></li>

                                    </ul>
                                    
                                </div>
                                <hr style="background: red;">
                                <p class="about_t" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Qisqacha ma’lumot
                                    Kafolat muddati (oy): 12<br>
                                    
                                    Operatsion sistema versiyasi: Android 11<br>
                                    
                                    Doimiy xotira hajmi: 64GB<br>
                                    
                                    Face ID Datchigi: Mavjud<br>
                                    
                                    Ishlab chiqaruvchi: Vetnam<br>
                                    
                                    Siz Samsung Galaxy A32 4/64GB sotib olish uchun nafaqat Toshkent, balki Andijon, Buxoro, Farg'ona, Jizzax, Xorazm, Namangan, Navoiy, Qashqadaryo, Qoraqalpog'iston Respublikasi, Samarqand, Sirdaryo, Surxondaryo, Toshkent viloyatidan buyurtma berishingiz mumkin. Siz :brend muddatli to'lovga sotib olishingiz mumkin. Samsung Galaxy A32 4/64GB kreditga olib nima qilasiz, bo'lib to'lashga olish osonroq va tezroq bo'lsa? O'zbekiston bo'ylab juda tez yetkazib beramiz.</p>
                                <div class="sizes mt-5" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                    <div class="price d-flex flex-row align-items-center"> <span class="act-price">$20</span>
                                        <div class="ml-2"> <small class="dis-price">$59</small> <span>40% OFF</span> </div>
                                    </div>
                                    <h6 class="text-uppercase">Color</h6> 
                                    <div class="row">
                                        <label class="radio" style="background:black"> 
                                            <input type="radio" name="size" value="" checked>
                                            <span>black</span> 
                                        </label> 
                                        <label class="radio" style="background: blue;"> 
                                            <input type="radio" name="size" value="" checked>
                                            <span>blue</span>
                                        </label> 
                                        <label class="radio" style="background: white;"> 
                                            <input type="radio" name="size" value="">
                                            <span>white</span>
                                        </label> 
                                        <label class="radio" style="background: DarkBlue;"> 
                                            <input type="radio" name="size" value=""> 
                                            <span>DarkBlue</span> 
                                        </label> 
                                        <label class="radio" style="background: red;"> 
                                            <input type="radio" name="size" value=""> 
                                            <span>red</span> 
                                        </label>
                                    </div>
                                </div>
                                <div class="cart mt-4 align-items-center"> <a href="/site/card"><button class="btn btn-danger text-uppercase mr-2 px-4">Add to cart</button></a> <i class="fa fa-heart text-muted"></i> <i class="fa fa-share-alt text-muted"></i> </div>
                                <h3 class="mt-3" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"> O'zbekiston buylab yetkaziberish xizmati</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <!--comentare-->

    <div class="container mb-5 mt-5">
        <div class="row">
            <div class="panel panel-default widget">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span>
                    <h3 class="panel-title" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Xabar</h3>
                    <span class="label label-info">
                        78</span>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-xs-2 col-md-1" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                    <img src="https://avatarmaker.net/images/1.png" class="img-circle img-responsive" alt="" /></div>
                                <div class="col-xs-10 col-md-11">
                                    <div>
                                        <a href="http://www.jquery2dotnet.com/2013/10/google-style-login-page-desing-usign.html">
                                            Google Style Login Page Design Using</a>
                                        <div class="mic-info" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                            By: <a href="#">Shamshod </a> on  30 Iyul  2021
                                        </div>
                                    </div>
                                    <div class="comment-text" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                        Awesome design
                                    </div>
                                    
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-xs-2 col-md-1" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                    <img src="https://i.pinimg.com/originals/7f/79/6d/7f796d57218d9cd81a92d9e6e8e51ce4.png" class="img-circle img-responsive" alt="" /></div>
                                <div class="col-xs-10 col-md-11">
                                    <div>
                                        <a href="http://bootsnipp.com/BhaumikPatel/snippets/Obgj">Admin Panel Quick Shortcuts</a>
                                        <div class="mic-info" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                            By: <a href="#">RedFox</a> on 30 Iyul 2021
                                        </div>
                                    </div>
                                    <div class="comment-text" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                        euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim
                                    </div>
                                    
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-xs-2 col-md-1" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                    <img src="https://i.pinimg.com/originals/0f/61/91/0f6191cd0b4177c1a1b5e0ea2f872148.png" class="img-circle img-responsive" alt="" /></div>
                                <div class="col-xs-10 col-md-11">
                                    <div data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                        <a href="http://bootsnipp.com/BhaumikPatel/snippets/4ldn">Cool Sign Up</a>
                                        <div class="mic-info">
                                            By: <a href="#">Odil</a> on  25 Iyul  2021
                                        </div>
                                    </div>
                                    <div class="comment-text" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                        euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim
                                    </div>
                                    
                                </div>
                            </div>
                        </li>
                    </ul>
                    <a href="#" class="btn btn-primary btn-sm btn-block" role="button" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><span class="glyphicon glyphicon-refresh"></span> More</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });

            $(".zoom").hover(function() {

                $(this).addClass('transition');
            }, function() {

                $(this).removeClass('transition');
            });
        });

        function change_image(image){

        var container = document.getElementById("main-image");

        container.src = image.src;
        }



        document.addEventListener("DOMContentLoaded", function(event) {


        });
    </script>