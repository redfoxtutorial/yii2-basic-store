<div class="brand_color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="titlepage">
                            <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Tanlangan mahsulotlar</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="container mt-5 mb-5 p-3 rounded cart">
            <div class="row no-gutters">
                <div class="col-md-8">
                    <div class="product-details mr-2">
                        <div class="d-flex flex-row align-items-center"> <a href="brand.html"><i class="fa fa-long-arrow-left"></i><span class="ml-2">Orqaga</span></a></div>
                        <hr>
                        <h6 class="mb-0" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Shopping cart</h6>
                        <div class="d-flex justify-content-between" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><span>You have 4 items in your cart</span>
                            <div class="d-flex flex-row align-items-center"><span class="text-black-50">Sort by:</span>
                                <div class="price ml-2"><span class="mr-1">price</span><i class="fa fa-angle-down"></i></div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="d-flex flex-row"><img class="rounded" src="https://cdn.goodzone.uz/goodzone/4e2093fd-50f8-4185-aee3-c0f8ad44476f" width="40">
                                <div class="ml-2"><span class="font-weight-bold d-block">Iphone 11 pro</span><span class="spec">256GB, Navy Blue</span></div>
                            </div>
                            <div class="d-flex flex-row align-items-center"><span class="d-block">2</span><span class="d-block ml-5 font-weight-bold">$900</span><i class="fa fa-trash-o ml-3 text-black-50"></i></div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="d-flex flex-row"><img class="rounded" src="https://avatars.mds.yandex.net/get-mpic/4415357/img_id4311518799121644816.jpeg/14hq" width="40">
                                <div class="ml-2"><span class="font-weight-bold d-block">One pro 7T</span><span class="spec">256GB, Navy Blue</span></div>
                            </div>
                            <div class="d-flex flex-row align-items-center"><span class="d-block">2</span><span class="d-block ml-5 font-weight-bold">$900</span><i class="fa fa-trash-o ml-3 text-black-50"></i></div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="d-flex flex-row"><img class="rounded" src="https://avatars.mds.yandex.net/get-mpic/4076910/img_id2690313567866349770.jpeg/orig" width="40">
                                <div class="ml-2"><span class="font-weight-bold d-block">Google pixel 4 XL</span><span class="spec">256GB, Axe black</span></div>
                            </div>
                            <div class="d-flex flex-row align-items-center"><span class="d-block">1</span><span class="d-block ml-5 font-weight-bold">$800</span><i class="fa fa-trash-o ml-3 text-black-50"></i></div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="d-flex flex-row"><img class="rounded" src="https://fan-android.com/uploads/posts/2019-08/1565293545_samsung-galaxy-note10-1565212599-0-12.png" width="40">
                                <div class="ml-2"><span class="font-weight-bold d-block">Samsung galaxy Note 10&nbsp;</span><span class="spec">256GB, Navy Blue</span></div>
                            </div>
                            <div class="d-flex flex-row align-items-center"><span class="d-block">1</span><span class="d-block ml-5 font-weight-bold">$999</span><i class="fa fa-trash-o ml-3 text-black-50"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="payment-info" >
                        <div class="d-flex justify-content-between align-items-center"><span>Card details</span><img class="rounded" src="https://www.twowaymirrors.com/wp-content/uploads/2018/06/max-braun-avatar.jpg" width="30"></div><span class="type d-block mt-3 mb-1">Card type</span><label class="radio"> <input type="radio" name="card" value="payment" checked> <span><img width="30" src="https://img.icons8.com/color/48/000000/mastercard.png" /></span> </label>
                        <label class="radio"> <input type="radio" name="card" value="payment"> <span><img width="30" src="https://img.icons8.com/officel/48/000000/visa.png" /></span> </label>
                        <label class="radio"> <input type="radio" name="card" value="payment"> <span><img width="30" src="https://img.icons8.com/ultraviolet/48/000000/amex.png" /></span> </label>
                        <label class="radio"> <input type="radio" name="card" value="payment"> <span><img width="30" src="https://img.icons8.com/officel/48/000000/paypal.png" /></span> </label>
                        <div><label class="credit-card-label">Name on card</label><input type="text" class="form-control credit-inputs" placeholder="Name"></div>
                        <div><label class="credit-card-label">Card number</label><input type="text" class="form-control credit-inputs" placeholder="0000 0000 0000 0000"></div>
                        <div class="row" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="col-md-6"><label class="credit-card-label" >Date</label><input type="text" class="form-control credit-inputs" placeholder="12/24"></div>
                            <div class="col-md-6"><label class="credit-card-label">CVV</label><input type="text" class="form-control credit-inputs" placeholder="342"></div>
                        </div>
                        <hr class="line">
                        <div class="d-flex justify-content-between information" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><span>Subtotal</span><span>$3000.00</span></div>
                        <div class="d-flex justify-content-between information" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><span>Shipping</span><span>$20.00</span></div>
                        <div class="d-flex justify-content-between information" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><span>Total(Incl. taxes)</span><span>$3020.00</span></div><button class="btn btn-primary btn-block d-flex justify-content-between mt-3" type="button"><span>$3020.00</span><span>Checkout<i class="fa fa-long-arrow-right ml-1"></i></span></button>
                    </div>
                </div>
            </div>
        </div>