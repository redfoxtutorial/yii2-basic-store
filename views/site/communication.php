<div class="brand_color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titlepage">
                    <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Aloqa</h2>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- contact -->
<div class="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <form class="main_form">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <input class="form-control" placeholder="Your name" type="text" name="Your Name">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <input class="form-control" placeholder="Email" type="text" name="Email">
                        </div>
                        <div class=" col-md-12" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <input class="form-control" placeholder="Phone" type="text" name="Phone">
                        </div>
                        <div class="col-md-12" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <textarea class="textarea" placeholder="Message"></textarea>
                        </div>
                        <div class=" col-md-12">
                            <button class="send" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end contact -->