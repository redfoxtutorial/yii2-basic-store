<?php

/* @var $this yii\web\View */

use yii\base\InvalidConfigException;

$this->title = 'E-store home';
try {
    $this->registerJsFile('@web/js/jquery.min.js');
} catch (InvalidConfigException $e) {

}
?>
<section class="slider_section">
    <div id="myCarousel" class="carousel slide banner-main" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide" src="images/banner.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption relative">
                        <span>Yangi mahsulotlar </span>
                        <h1> 25% chegirmalar asosida</h1>
                        <p>Biz sifatni qadirlaymiz</p>
                        <a class="buynow" href="brand.html">Sotib olish</a>
                        <ul class="social_icon">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide" src="/images/banner.jpg" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption relative">
                        <span>All New Phones </span>
                        <h1>up to 25% Flat Sale</h1>
                        <p>It is a long established fact that a reader will be distracted by the readable content
                            <br> of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                        <a class="buynow" href="#">Buy Now</a>
                        <ul class="social_icon">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide" src="/images/banner.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption relative">
                        <span>All New Phones </span>
                        <h1>up to 25% Flat Sale</h1>
                        <p>It is a long established fact that a reader will be distracted by the readable content
                            <br> of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                        <a class="buynow" href="brand.html">Buy Now</a>
                        <ul class="social_icon">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- about -->
<div class="about">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5 co-sm-l2">
                <div class="about_img" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <figure><img src="/images/about.png" alt="img" /></figure>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-7 co-sm-l2">
                <div class="about_box"  data-aos="fade-up" data-aos-anchor-placement="bottom-bottom" >
                    <span>Our Mobile Shop</span>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of It is a long established fact that a reader will be distracted by the </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end about -->


<!-- clients -->
<div class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titlepage"  data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <h2>what say our clients</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clients_red">
    <div class="container">
        <div id="carousel slide" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#testimonial_slider" data-slide-to="0" class=""></li>
                <li data-target="#testimonial_slider" data-slide-to="1" class="active"></li>
                <li data-target="#testimonial_slider" data-slide-to="2" class=""></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item">
                    <div class="testomonial_section">
                        <div class="full center">
                        </div>
                        <div class="full testimonial_cont text_align_center cross_layout">
                            <div class="cross_inner"  >
                                <h3>Due markes<br><strong class="ornage_color">Rental</strong></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess</i>
                                </p>
                                <div class="full text_align_center margin_top_30">
                                    <img src="/icon/testimonial_qoute.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item active">

                    <div class="testomonial_section">
                        <div class="full center">
                        </div>
                        <div class="full testimonial_cont text_align_center cross_layout">
                            <div class="cross_inner">
                                <h3>Due markes<br><strong class="ornage_color">Rental</strong></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess</i>
                                </p>
                                <div class="full text_align_center margin_top_30">
                                    <img src="/icon/testimonial_qoute.png">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="carousel-item">

                    <div class="testomonial_section">
                        <div class="full center">
                        </div>
                        <div class="full testimonial_cont text_align_center cross_layout">
                            <div class="cross_inner">
                                <h3>Due markes<br><strong class="ornage_color">Rental</strong></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess</i>
                                </p>
                                <div class="full text_align_center margin_top_30">
                                    <img src="/icon/testimonial_qoute.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
<!-- end clients -->


<div class="container-fluid">
    <div class="row__a"  >
        <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Yangi mahsulotlarimiz </h2>
    </div>

</div>

<div class="container mt-5" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
    <div class="row">
        <div class="four col-md-3 col-lg-3 mb-1">
            <div class="counter-box colored"> <i class="fa fa-thumbs-o-up"></i> <span class="counter">2147</span>
                <p>Happy Customers</p>
            </div>
        </div>
        <div class="four col-md-3 col-lg-3 mb-1" >
            <div class="counter-box"> <i class="fa fa-group"></i> <span class="counter">3275</span>
                <p>Registered Members</p>
            </div>
        </div>
        <div class="four col-md-3 col-lg-3 mb-1">
            <div class="counter-box"> <i class="fa fa-shopping-cart"></i> <span class="counter">289</span>
                <p>Available Products</p>
            </div>
        </div>
        <div class="four col-md-3">
            <div class="counter-box"> <i class="fa fa-user"></i> <span class="counter">1563</span>
                <p>Saved Trees</p>
            </div>
        </div>

    </div>
</div>

<!----------card --------->
<div class="container-fluid mt-5 mb-5">
    <div class="row g-2">
        <div class="col-md-3">
            <div class="t-products p-2">
                <h6 class="text-uppercase" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Computer & Periferals</h6>
                <div class="p-lists"  data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="d-flex justify-content-between mt-2"> <span>Laptops</span> <span>23</span> </div>
                    <div class="d-flex justify-content-between mt-2"> <span>Desktops</span> <span>46</span> </div>
                    <div class="d-flex justify-content-between mt-2"> <span>Monitors</span> <span>13</span> </div>
                    <div class="d-flex justify-content-between mt-2"> <span>Mouse</span> <span>33</span> </div>
                    <div class="d-flex justify-content-between mt-2"> <span>Keyboard</span> <span>12</span> </div>
                    <div class="d-flex justify-content-between mt-2"> <span>Printer</span> <span>53</span> </div>
                    <div class="d-flex justify-content-between mt-2"> <span>Mobiles</span> <span>203</span> </div>
                    <div class="d-flex justify-content-between mt-2"> <span>CPU</span> <span>23</span> </div>
                </div>
            </div>
            <div class="processor p-2"  data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                <div class="heading d-flex justify-content-between align-items-center">
                    <h6 class="text-uppercase">Processor</h6> <span>--</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"> <label class="form-check-label font_fam" for="flexCheckDefault"> Intel Core i7 </label> </div> <span>3</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Intel Core i6 </label> </div> <span>4</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Intel Core i3 </label> </div> <span>14</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Intel Centron </label> </div> <span>8</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Intel Pentinum </label> </div> <span>14</span>
                </div>
            </div>
            <div class="brand_ba p-2" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                <div class="heading d-flex justify-content-between align-items-center">
                    <h6 class="text-uppercase">Brand</h6> <span>--</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"> <label class="form-check-label font_fam" for="flexCheckDefault"> Apple </label> </div> <span>13</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Asus </label> </div> <span>4</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Dell </label> </div> <span>24</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Lenovo </label> </div> <span>18</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Acer </label> </div> <span>44</span>
                </div>
            </div>
            <div class="type p-2 mb-2"  data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                <div class="heading d-flex justify-content-between align-items-center">
                    <h6 class="text-uppercase">Type</h6> <span>--</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"> <label class="form-check-label font_fam" for="flexCheckDefault"> Hybrid </label> </div> <span>23</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Laptop </label> </div> <span>24</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Desktop </label> </div> <span>14</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Touch </label> </div> <span>28</span>
                </div>
                <div class="d-flex justify-content-between mt-2">
                    <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked> <label class="form-check-label font_fam" for="flexCheckChecked"> Tablets </label> </div> <span>44</span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-9 col-lg-9 ">
            <div class="row g-2">
                <div class="col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>
                            <a href="/site/brand"><button data-aos="fade-up" data-aos-anchor-placement="bottom-bottom" type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>  <a href="brand.html"><button  type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>  <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div> <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>  <a href="brand.html"><button  type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>  <a href="brand.html"><button  type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>  <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>  <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i>Harid qilish</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mt-2">
                    <div class="card">
                        <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                        </div>
                        <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                            </div>
                            <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                            <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                            <div class="text-muted mb-3">34 reviews</div>  <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------card end---------------->
<!-- Pagination-->
<div class="demo mb-5">
    <nav class="pagination-outer" aria-label="Page navigation">
        <ul class="pagination">
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                <a href="#" class="page-link" aria-label="Previous">
                    <span aria-hidden="true">«</span>
                </a>
            </li>
            <li class="page-item active"  data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">1</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">2</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">3</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">4</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">5</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"> <a href="#" class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a>
            </li>
        </ul>
    </nav>
</div>