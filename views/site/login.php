<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="site-login">
    <div class="brand_color">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage">
                        <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><?= Html::encode($this->title) ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n<div>{input}</div>\n<div>{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-1 control-label'],
                        ],
                    ]); ?>
                    
                    <div class=" col-md-12">
                        <p>Please fill out the following fields to login:</p>
                        <br />
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class=" col-md-12">
                        <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                    <div class=" col-md-12">
                        <?= $form->field($model, 'rememberMe')->checkbox([
                            'template' => "<div>{input} {label}</div>\n<div>{error}</div>",
                        ]) ?>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3 ml-3">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= Html::a('Registration', ['site/registration']) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

