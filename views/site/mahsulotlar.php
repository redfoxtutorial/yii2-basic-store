
<!-- end header -->
<section class="slider_section">
    <div id="myCarousel" class="carousel slide banner-main" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide" src="/web/images/banner.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption relative">
                        <span>All New Phones </span>
                        <h1>up to 25% Flat Sale</h1>
                        <p>It is a long established fact that a reader will be distracted by the readable content
                            <br> of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                        <a class="buynow" href="#">Buy Now</a>

                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide" src="/web/images/banner1.png" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption relative">
                        <span>All New Phones </span>
                        <h1>up to 50% Flat Sale</h1>
                        <p>It is a long established fact that a reader will be distracted by the readable content
                            <br> of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                        <a class="buynow" href="#">Buy Now</a>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="container-fluid">
    <div class="row__b" >
        <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Yangi mahsulotlarimiz </h2>
    </div>

</div>

<div class="container mt-5">
    <div class="row">
        <div class="four col-md-3 col-lg-3">
            <div class="counter-box colored" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"> <i class="fa fa-thumbs-o-up"></i> <span class="counter">2147</span>
                <p>Happy Customers</p>
            </div>
        </div>
        <div class="four col-md-3 col-lg-3" >
            <div class="counter-box" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"> <i class="fa fa-group"></i> <span class="counter">3275</span>
                <p>Registered Members</p>
            </div>
        </div>
        <div class="four col-md-3 col-lg-3">
            <div class="counter-box" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"> <i class="fa fa-shopping-cart"></i> <span class="counter">289</span>
                <p>Available Products</p>
            </div>
        </div>
        <div class="four col-md-3">
            <div class="counter-box " data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"> <i class="fa fa-user"></i> <span class="counter">1563</span>
                <p>Saved Trees</p>
            </div>
        </div>

    </div>
</div>


<!----------card --------->

<div class="container container__b">
    <div class="row">
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div><a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i>Harid qilish</button></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>

                    <div class="text-muted mb-3">34 reviews</div>
                    <a class="btn btn-warning add-to-card" data-status="0"><i class="fa fa-cart-plus mr-2"></i></a>

                    <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i>Harid qilish</button></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div>

                    <a class="btn btn-warning add-to-card" data-status="0"><i class="fa fa-cart-plus mr-2"></i></a>

                    <a href="brand.html"><button type="button" class="btn bg-cart">
                            <i class="fa fa-cart-plus mr-2"></i> Harid qilish</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div> <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div> <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div> <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div> <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div> <a href="brand.html"><button type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="card-img-actions"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1562074043/234.png" class="card-img img-fluid" width="96" height="350" alt=""> </div>
                </div>
                <div class="card-body bg-light text-center" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <div class="mb-2">
                        <h6 class="font-weight-semibold mb-2"> <a href="#" class="text-default mb-2" data-abc="true">Toshiba Notebook with 500GB HDD & 8GB RAM</a> </h6> <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a>
                    </div>
                    <h3 class="mb-0 font-weight-semibold">$250.99</h3>
                    <div> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> </div>
                    <div class="text-muted mb-3">34 reviews</div> <a href="brand.html"><button data-aos="fade-up" data-aos-anchor-placement="bottom-bottom" type="button" class="btn bg-cart"><i class="fa fa-cart-plus mr-2"></i> Harid qilish</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------card end---------------->

<!-- Pagination-->
<div class="demo mt-5">
    <nav class="pagination-outer" aria-label="Page navigation">
        <ul class="pagination">
            <li class="page-item">
                <a href="#" class="page-link" aria-label="Previous" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                    <span aria-hidden="true">«</span>
                </a>
            </li>
            <li class="page-item active" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">1</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">2</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">3</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">4</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><a class="page-link" href="#">5</a></li>
            <li class="page-item" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                <a href="#" class="page-link" aria-label="Next">
                    <span aria-hidden="true">»</span>
                </a>
            </li>
        </ul>
    </nav>
</div>
<!-- Pagination end-->
