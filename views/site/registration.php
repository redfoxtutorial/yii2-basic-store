<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="brand_color">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage">
                        <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom"><?= Html::encode($this->title) ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    
                    <?php $form = ActiveForm::begin(); ?>                    
                
                    <div class=" col-md-12">
                        <?= $form->field($model, 'full_name')->textInput(['autofocus' => true]) ?>
                    </div>

                    <div class=" col-md-12">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    </div>

                    <div class=" col-md-12">
                        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                    </div>

                    <div class=" col-md-12">
                        <?= $form->field($model, 'password')->textInput(['autofocus' => true]) ?>
                    </div>

                    <div class=" col-md-12">
                        <?= $form->field($model, 'gender')->dropDownList(ArrayHelper::map(Yii::$app->params['gender'], 'key', 'name'),['prompt' => 'Tanlang','class'=>'form-control']); ?>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-1 col-lg-11">
                            <?= Html::submitButton('Registration', ['class' => 'btn btn-primary', 'name' => 'registration-button']) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

