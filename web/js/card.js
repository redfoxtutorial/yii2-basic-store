$(document).ready(function (){
    $('.add-to-card').click(function (){
        var old = $('.card-product').text();
        if($(this).attr('data-status') == 0){
            if(old){
                $('.card-product').text(parseInt(old)+1);
            } else {
                $('.card-product').text(1);
            }
            $(this).attr('data-status',1)
        } else if($(this).attr('data-status') == 1) {
            if(old){
               let  result = parseInt(old)-1;
                if(result == 0){
                  //  $('.card-product').text('');
                } else {
                    $('.card-product').text(result);
                }
            }
            $(this).attr('data-status',0)
        }
    });
});