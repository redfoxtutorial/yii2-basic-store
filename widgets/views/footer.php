<footer>
    <div id="contact" class="footer">
        <div class="container">
            <div class="row pdn-top-30">
                <div class="col-md-12 ">
                    <div class="footer-box">
                        <div class="headinga">
                            <h3 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Address</h3>
                            <span data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">RedFox IT company, пр-д Архитекторов 11.</span>
                            <p data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">(+71) 8522369417
                                <br>demo@gmail.com</p>
                        </div>
                        <ul class="location_icon" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>

                        </ul>
                        <div class="menu-bottom" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
                            <ul class="link">
                                <li> <a href="#">Bosh sahifa</a></li>
                                <li> <a href="#">biz haqimizda</a></li>
                                <li> <a href="#">Mahsulotlar  </a></li>
                                <li> <a href="#">Aloqa</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <p>© 2019 All Rights Reserved. Design By <a href="https://redfox.uz/"> REDFOX</a></p>
            </div>
        </div>
    </div>
</footer>