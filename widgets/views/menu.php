<?php
use yii\helpers\Html;

?>
<style>
    .dropdown-item{
        color: black !important;
    }
</style>
<header>
    <!-- header inner -->
    <div class="header">
        <div class="parallelogram"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="menu-area">
                        <div class="limit-box">
                            <li class="">
                                <a href="/"><img src="/images/logo.svg" style="height: 20px" alt=""></a>
                            </li>
                            <nav class="main-menu">
                                <ul class="menu-area-main">

                                    <li class="active"> 
                                        <?= Html::a(Yii::t('lang','Home page'),'/') ?>  
                                    </li>
                                    <li class="active">
                                        <?= Html::a('Biz haqimizda','/site/aboutus') ?>
                                    </li>
                                    <li class="active">
                                        <?= Html::a('Mahsulotlar','/site/mahsulotlar') ?>
                                    </li>
                                    <li class="active">
                                        <?= Html::a('Aloqa','/site/communication') ?>
                                    </li>


                                    <li>
                                        <a href="/site/account">
                                            <i class="fa fa-user"></i>
                                        </a>
                                        <?php
                                            if(!Yii::$app->user->isGuest){
                                               echo Html::beginForm(['/site/logout'],'post')
                                                . Html::submitButton('Logout')
                                                . Html::endForm();
                                            }
                                        ?>
                                    </li>
                                    <li class="last">
                                       <a class="position-relative text-white">
                                           <i class="fa fa-shopping-cart"></i>
                                           <span class="d-block position-absolute  card-product" style="top: 15px; right: -10px;"></span>
                                       </a>

                                    </li>
                                    <li class="nav-item dropdown ">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php
                                            if(\Yii::$app->language == 'uz'):
                                                echo 'UZ';
                                            elseif(\Yii::$app->language == 'ru'):
                                                echo 'RU';
                                            endif;
                                            ?>
                                        </a>
                                        <div class="dropdown-menu " style="min-width: 70px !important;" aria-labelledby="navbarDropdown">
                                            <?php
                                            if(\Yii::$app->language == 'uz'):
                                                echo Html::a('RU', array_merge(
                                                    \Yii::$app->request->get(),
                                                    [\Yii::$app->controller->route, 'language' => 'ru']
                                                ),['class' => 'dropdown-item p-2']);
                                            elseif(\Yii::$app->language == 'ru'):
                                                echo Html::a('UZ', array_merge(
                                                    \Yii::$app->request->get(),
                                                    [\Yii::$app->controller->route, 'language' => 'uz']
                                                ),['class' => 'dropdown-item p-2']);
                                            endif;
                                            ?>
                                        </div>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 offset-md-6">
                    <div class="location_icon_bottum">
                        <ul>
                            <li><i class="fa fa-phone mr-1"></i>(+99)9876543109</li>
                            <li><i class="fa fa-envelope mr-1"></i>demo@gmail.com</li>
                            <li><i class="fa fa-map-marker mr-1"></i>Location</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end header inner -->
</header>